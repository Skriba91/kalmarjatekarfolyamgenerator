import java.util.ArrayList;
import java.util.List;

//Ez t�rolja egy v�ros nev�t, �s az adott �rucikkeket �s az �rucikkek �rfolyam�t
public class City {

	private String name;
	private List<Comodity> comoditys;
	
	public City(String name, List<Comodity> comoditys) {
		this.name = name;
		this.comoditys = comoditys;
	}
	
	public void setComodityPrice(Comodity comodity, int sell, int buy) {
		int index = comoditys.indexOf(comodity);
		Comodity comodityWithPrice = comoditys.get(index);
		comodityWithPrice.setPrice(sell, buy);
		comoditys.set(index, comodityWithPrice);
	}
	
	public String getName() {
		return name;
	}
	
	public boolean equals(City city) {
		if(name.equals(city.getName()))
			return true;
		else
			return false;
	}
	
}
