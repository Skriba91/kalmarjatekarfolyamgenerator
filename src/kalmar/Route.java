package kalmar;


import java.util.HashMap;
import java.util.Map;

public class Route {
	
	private class RouteStation {
		City stationCity;
		Comodity stationComodity;
		
		public RouteStation(City stationCity, Comodity stationComodity) {
			this.stationCity = stationCity;
			this.stationComodity = stationComodity;
		}
		
		public boolean equals(City city, Comodity comodity) {
			if(stationCity.equals(city) || stationComodity.equals(comodity))
				return true;
			else
				return false;
		}
		
		public boolean identical(City city, Comodity comodity) {
			if(stationCity.equals(city) && stationComodity.equals(comodity))
				return true;
			else
				return false;
		}
		
		public City getCity() {
			return stationCity;
		}
		
		public Comodity getComodity() {
			return stationComodity;
		}
		
	}
	//T�rolja 
	private Map<Integer,RouteStation> stations = new HashMap<>();
	
	public void addStation(int pos, City city, Comodity comodity) {
		stations.put(pos, new RouteStation(city, comodity));
		
	}
	
	/**
	 * Ellen�rzi, hogy az adott karav�n�tban van-e m�r a megadott v�ros vagy �rucikk
	 * Checks the route 
	 * @param city 
	 * @param comodity
	 * @return
	 */
	public boolean isElement(City city, Comodity comodity) {
		for(RouteStation n : stations.values()) {
			if(n.equals(city, comodity))
				return true;
		}
		return false;
	}
	
	/**
	 * Ellen�rzi, hogy az az adott v�ros �rucikk komb� l�tezik-e m�r
	 * @param city
	 * @param comodity
	 * @return
	 */
	public boolean isComodityUsed(City city, Comodity comodity) {
		for(RouteStation n : stations.values()) {
			if(n.identical(city, comodity))
				return true;
		}
		return false;
	}
	
	
	/**
	 * Ellen�rzi, hogy az adott �tvonal l�tezik-e m�r (valahogy)
	 * @return
	 */
	public boolean isExistingRoute(Route route) {
		for(int i = 0; i < route.size()-1; ++i) {
			for(int j = 0; j < stations.size()-1; ++j) {
				if(route.get(i).equals(stations.get(j)) && route.get(i+1).equals(stations.get(j+1)))
					return true;
			}
		}
		return false;
	}
	
	public int size() {
		return stations.size();
	}
	
	public RouteStation get(int key) {
		return stations.get(key);
	}
	
	public City getCity(int key) {
		return get(key).getCity();
	}
	
	public Comodity getComodity(int key) {
		return get(key).getComodity();
	}

}
