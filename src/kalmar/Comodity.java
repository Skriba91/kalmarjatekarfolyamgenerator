//Egy �rucikket nev�t tratalmaz� oszt�ly az elad�si �s v�teli �r�val egy�tt
public class Comodity {
	
	private String name;
	private int sellPrice;
	private int buyPrice;
	private boolean priceSet;
	
	public Comodity(String name) {
		this.name = name;
		sellPrice = 0;
		buyPrice = 0;
		priceSet = false;
	}
	
	public Comodity(String name, int sell, int buy) {
		this.name = name;
		sellPrice = sell;
		buyPrice = buy;
		priceSet = true;
	}
	
	/**
	 * Setter for comodity name
	 * @param name The name that will be set for the comodity
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter for the comofity name
	 * @return The name of the comofity
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter for the sell price of the comodity
	 * @param price The price that will be set for the comodity
	 */
	public void setSellPrice(int price) {
		sellPrice = price;
	}
	
	/**
	 * Getter for the sell price of the commodity
	 * @return The sell price of the comodity
	 */
	public int getSellPrice() {
		return sellPrice;
	}
	
	/**
	 * Setter for the buy price of the comodity
	 * @param price The buy price that will be set for the comodity
	 */
	public void setBuyPrice(int price) {
		buyPrice = price;
	}
	
	/**
	 * Getter for the buy price of the comodity
	 * @return The buy price with the comodity
	 */
	public int getBuyPrice(){
		return buyPrice;
	}
	
	/**
	 * Setts both buy and sell prices for the comodity
	 * @param sell The sell price that will be set for the comodity
	 * @param buy The buy price that will be set for the comodity
	 */
	public void setPrice(int sell, int buy) {
		sellPrice = sell;
		buyPrice = buy;
		priceSet = true;
	}
	
	/**
	 * It say that is there a valid price set for the comodity
	 * @return True if valid price is sety, false if no valid price is set
	 */
	public boolean isPiceSet() {
		return priceSet;
	}
	
	public boolean equals(Comodity comodity) {
		if(name.equals(comodity.getName()))
			return true;
		else
			return false;
	}

}
