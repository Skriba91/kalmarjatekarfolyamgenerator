import java.util.ArrayList;
import java.util.List;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.util.Random;

import javax.lang.model.element.Element;

import utility.FileIO;

public class Pricing {
	
	private final int BASE = 100;
	
	private List<City> citys  = new ArrayList<>();
	private List<Comodity> comoditys  = new ArrayList<>();
	private List<Route> routs = new ArrayList<>();
	private final int NUMBERSOFROUNDS = 6;
	private final int MAXROUTELENGHT = 5;
	private int base = BASE;
	
	public void initGame() {
		List<String> inputCity = FileIO.loadCitys();
		List<String> inputComodity = FileIO.loadComoditys();
		for(String n : inputComodity) {			//�rulista felv�tele �rak n�lk�l
			comoditys.add(new Comodity(n));
		}
		for(String n: inputCity) {
			citys.add(new City(n,comoditys));
		}
	}
	
	public void makePrices() {
		for(Route n : routs) {
			City city;
			Comodity comodity;
			for(int i = 0; i < n.size(); ++i) {
				city = n.getCity(i);
				comodity = n.getComodity(i);
				int index = citys.indexOf(city);
				
			}
		}
	}
	
	public void makeRouts() {
				/**
		 * A legr�videbb karav�t �t k�t v�rost �rint
		 */
		for(int i = 2; i < MAXROUTELENGHT; ++i) {	//K�l�nb�z� karav�nutak l�trehoz�sa
			Route route;
			do {
				route = makeRoute(i);
			} while (checkExistringRoutes(route));
			routs.add(route);
			
			
			
		}
	}
	
	public Route makeRoute(int lenght) {
		Random r = new Random(calculateSeed());
		Route route = new Route();
		City city;
		Comodity comodity;
		for(int j = 0; j < lenght; ++j) {			//Egy adot karav�n �t l�trehoz�sa
			if(j == 0) {	//M�g nem kell semmit ellen�rizni �s hozz� kell 
				city = citys.get(r.nextInt(citys.size()));
				comodity = comoditys.get(r.nextInt(comoditys.size()));
				route.addStation(j, city, comodity);
			}
			else {	//Ellen�rizni kell, hogy van-e ilyen �t
				boolean element;
				boolean used;
				do {
					city = citys.get(r.nextInt(citys.size()));
					comodity = comoditys.get(r.nextInt(comoditys.size()));
					element = route.isElement(city, comodity);
					used = checkComodityIsUsed(city, comodity);
					
				} while (element || used);
				route.addStation(j, city, comodity);
			}
		}
		return route;
	}
	
	/**
	 * Ellen�rzi, hogy az eddig l�trehozott kereskedelmi �tvonalak k�z�tt szerepel-e m�r az �j �tvonal
	 * @param route
	 * @return
	 */
	public boolean checkExistringRoutes(Route route) {
		for(Route n : routs) {
			if(n.isExistingRoute(route))
				return true;
		}
		return false;
	}
	
	public boolean checkComodityIsUsed(City city, Comodity comodity) {
		for(Route n : routs) {
			if(n.isComodityUsed(city, comodity))
				return true;
		}
		return false;
	}
	
	//TODO Random funkci�knak k�l�n oszt�ly
	private long calculateSeed() {
		Point P;
		PointerInfo a;
		a = MouseInfo.getPointerInfo();
		P = a.getLocation();
		Random r1 = new Random(System.currentTimeMillis() + Double.doubleToLongBits(P.getX() + P.getY()));
		long seed; //The final seed for the random number
		Runtime runtime = Runtime.getRuntime();		//Memory data to calculate final seed
		long totalMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();
		long maxMemory = runtime.maxMemory();
		long usedMemory = totalMemory - freeMemory;
		if(r1.nextInt() < r1.nextInt()) {
			a = MouseInfo.getPointerInfo();
			P = a.getLocation();
			double seedValue = 1586.6 - 708.75;			//Temp variable for seed
			Random r2 = new Random(System.currentTimeMillis()*r1.nextLong() + Double.doubleToLongBits(P.getX()));
			seedValue *= Math.sin((totalMemory*usedMemory)/r2.nextDouble());
			seedValue *= Math.cos((maxMemory/freeMemory)*r2.nextDouble());
			seedValue = Math.abs(seedValue) + (P.getX() + P.getY()) + 1;
			seed = Double.doubleToLongBits(seedValue);
		}
		else {
			a = MouseInfo.getPointerInfo();
			P = a.getLocation();
			double seedValue = 1586.6 - 708.75;			//Temp variable for seed
			Random r2 = new Random(System.currentTimeMillis()*r1.nextLong() + Double.doubleToLongBits(P.getY()));
			seedValue *= Math.cos((totalMemory/usedMemory)/r2.nextDouble());
			seedValue *= Math.sin((maxMemory*freeMemory)*r2.nextDouble());
			seedValue = Math.abs(seedValue) + (P.getX() + P.getY()) + 1;
			seed = Double.doubleToLongBits(seedValue);
			}
		
		return seed;
		}

}
