package utility;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import kalmar.City;

public class FileIO {
	
	private final static String VAROSOK = "varosok.txt";
	private final static String ARUCIKKEK = "arucikkek.txt";
	
	public static List<String> loadCitys() {
		Path fileName = Paths.get(VAROSOK);
		if(Files.exists(fileName)) {
			List<String> citys = new ArrayList<>();
			try {
				citys = Files.readAllLines(fileName);
				return citys;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		else
			return null;
		/*else {
			int participants;
			try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
				System.out.print("Give the number of participants: ");
				participants = Integer.parseInt(br.readLine());
				System.out.println("Give the name of participants.\nEvery name in a new line!");
				lot = new Lot();
				for(int i = 0; i < participants; ++i)
					lot.newName(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}*/
			

	}
	
	public static List<String> loadComoditys() {
		Path fileName = Paths.get(ARUCIKKEK);
		if(Files.exists(fileName)) {
			List<String> comoditys = new ArrayList<>();
			try {
				comoditys = Files.readAllLines(fileName);
				return comoditys;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		else
			return null;
		/*else {
			int participants;
			try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
				System.out.print("Give the number of participants: ");
				participants = Integer.parseInt(br.readLine());
				System.out.println("Give the name of participants.\nEvery name in a new line!");
				lot = new Lot();
				for(int i = 0; i < participants; ++i)
					lot.newName(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}*/
			

	}
	
	public static void savePrices(List<City> citys) {
		
	}

}
